DebConf14 Bursaries Team -- Notes
=================================

During the process of organizing and coordinating the Bursaries Team,
I was reminded of some of the challenges we have, specially, some
questions and considerations that are usually too late to try to
answer.  Perhaps, adding it to this repository might help in the
future, at a certain point this should go to the wiki, but I decided
to start at the misc repository.


Recommendations
---------------
 * Review the requests for sponsorship before start ranking, and if
   possible, send a request for updates one week before the end of
   the period to request sponsorship.  Usually, people forget to
   update prices, dates or even add comments saying that they will
   add more details later (and usually the system doesn't allow it).

 * Set the rules ahead of time, so people know what to expect, it
   provides more visibility.  There's the "cheating" aspect that you
   can read more on the considerations section below.

 * You need 3-4 weeks to get a consensus with a group of 10 people.
   It is really hard to do it in just 2 weeks, keep that in mind
   when planning the date to announce the results (and also to
   define the period for people to request sponsorship).

 * Avoid ranking travel, food and accommodation together, try to do
   it in separate queues, the job is easier and is also easier to
   compare/rank.


Considerations
--------------

 * If we answer all the questions here, could we automate the process?
   - Would people than "cheat" knowing how we the automated process
     rank them?  Some might be already cheating knowing parts of how
     the process works.  And by being more transparent, the "cheating"
     can still happen.

 * Does it matter "where you come from"?
   - Should we prioritize America vs. Europe vs. Asia depending on the
     year?  Some votes are based on the continent, but this is not a
     rule to be followed, and if it is, we could make the software
     help us sorting.

 * Does it matter if "you are willing to pay a part of your trip"?
   - Will we enforce it?  If you said you can pay 300 of 600, you got
     tickets for 500, should we sponsor only 200?

 * Does it matter if you are a "DebConf Newbie"?
   - Should Bursaries have a separate budget for Newbies?  We would
     need to create rules and keep track.

 * Should we care about "Diversity?
   - If yes, how do we prioritize/vote?  How to ensure we are
     considering all forms of diversity (e.g. gender/sex, cultural,
     age, economical).
     
 * Are we afraid people not involved are taking vacations paid by US?
   - Is this a real concern?  Do we keep records so the next year the
     team is aware?
   
 * Should we penalize people that do not provide all the answers?
   - Having Location/Country is really important to assess the travel
     costs, so we should enforce it, and make a call for updates.  If
     possible, we shouldn't allow people to register without "origin".

   
 * How much details do we want to know about why people can't afford?
   - Does it matter for Debian?  If we are a trust-based project, and
     a fellow member says it would be a burden, the details might be
     a really private topic.

 * What if people self-assess?
   - For example:
     + Tell us how much you were involved last year?  -2 -1 0 +1 +2
     + Tell us how much you plan to be involved next year?  -2 -1 0 +1 +2

 * Should we switch to percentage values instead of absolute values?
   - We will reimburse you up to X% of your travel expenses, if you
     save money, we save too, travel budget could roll over to next
     year.

 * Maybe different team members should rank on different dimensions?
   - "Group A" scores based on geography
   - "Group B" scores based on budget
   - "Group C" scores based on activity and team membership

 * Change to a different system/field?
   - Should be sponsored: yes or no
   - What priority: high, medium, low

 * Should we ask people if they accept partial sponsorship?
   - If we have a delayed queue, we could provide a smaller amount
     for a larger group, but not covering all that was requested.
