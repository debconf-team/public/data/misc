#!/usr/bin/ruby
# -*- coding: utf-8 -*-
#
# This script expects two files to be present (and sorry for the lack
# of consistence): One is specified as ARGV[1], and contains the
# result of he following query to Pentabarf (for the current
# conference, of course):
#
# SELECT person_id, COALESCE (public_name, COALESCE(first_name,'')||' '||
#     COALESCE(last_name,'')) AS name, nickname, country, person.email FROM
#     person JOIN conference_person USING (person_id) WHERE conference_id=5
#     AND reconfirmed IS TRUE ORDER BY name;
#
# The second one is calle 'fprs.txt' and includes a tab-separated
# list: Email address, full GPG keyID, order in the KSP list. It is
# matched by email to each person present in the first file.
require 'prawn'
require 'prawn/measurement_extensions'
require 'barby'
require 'barby/outputter/cairo_outputter'

# List of attendees, parsed from the specified source file
class Attendance < Array
  def initialize(file)
    File.open(file).readlines.each do |line|
      next if line =~ /^( person_id|-|\()/
      cols = line.chomp.split('|').map {|col| col.strip}
      self << Person.new(cols[0], cols[1], cols[2], cols[3], cols[4])
    end
  end
end

# Each of the attendees' information
class Person
  attr_accessor :id, :name, :nickname, :country, :email, :gpg
  def initialize(id, name, nickname, country, email)
    @id = id.to_i
    @name = name
    @nickname = nickname
    @country = country
    @email = email
    @gpg = Key.find_for(email)
  end
end

# GPG keys information is keyed by email address, and contains the
# full GPG fingerprint and the number in the KSP list (for people to
# quickly relate to a mutually agreed trustable document)
class Key
  def self.find_for(mail)
    @@keys[mail]
  end

  def self.populate
    @@keys = {}
    File.open('fprs.txt').readlines.each do |lin|
      col = lin.chomp.split(/\t/)
## Currently this will overwrite with random results for people with
## more than one key :-/
      @@keys[col[0]] = [col[1], col[2]]
    end
  end
end

# Ugliest and largest class here: Prepares the PDF printout using
# Prawn. I tried to make all measures "elastic" relative to the
# nametags' size
class Printout
  NametagWidth = 9.cm
  NametagHeight = 5.cm
  Margins = 0

  def initialize
    @pdf = Prawn::Document.new(:page_size => "A4", :page_layout => :portrait)
    @cols = ( (@pdf.bounds.right - 2*Margins) / NametagWidth).floor
    @rows = ( (@pdf.bounds.top - 2*Margins) / NametagHeight).floor
    @curr_col = 1
    @curr_row = 1
  end


  # Prepares the page, the grid/table of nametags, advancing through
  # them. Systemwide DejaVuSans font works for most (all?)
  # Eastern/Western European characters; WenQuanYiMicroHei is used for
  # Taiwanese names.
  def render(list)
    Prawn::Font.load(@pdf, '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf')
    Prawn::Font::TTF.load(@pdf, 'WenQuanYiMicroHei.ttf')
    @pdf.font '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf'
    @pdf.bounding_box([Margins, Margins],
                      :width => NametagWidth * @cols,
                      :height => NametagHeight * @rows) do
      list.each do |person|
        # PDF generation starts at bottom left - So, substract one
        # from @curr_col, not from @curr_row
        @pdf.bounding_box([@pdf.bounds.left + (NametagWidth * (@curr_col-1)),
                           @pdf.bounds.top + (NametagHeight * @curr_row)],
                          :width => NametagWidth,
                          :height => NametagHeight) do
          @pdf.stroke_bounds
          nametag_for(person, @pdf)
          next_position
        end
      end
    end

    @pdf.render_file 'nametags.pdf'
  end

  # Advance along the rows and columns of the page (left to right,
  # bottom to top).
  def next_position
    if @curr_col < @cols
      @curr_col += 1
    elsif @curr_row < @rows
      @curr_row += 1
      @curr_col = 1
    else
      @curr_row = 1
      @curr_col = 1
      @pdf.start_new_page
    end
  end

  def nametag_for(person, pdf)
    # Ugly special-casing! People with Chinese characters in their
    # names have to explicitly use a different font
    if person.id == 237 or person.id == 2061
      @pdf.font 'WenQuanYiMicroHei.ttf'
    else
      @pdf.font '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf'
    end

    # Avoid exceptions :-P
    person.name = '' if person.name.nil?
    person.nickname = '' if person.nickname.nil?
    puts '%s (%s)' % [person.name,person.nickname]

    # Include a nice logo at the bottom left
    pdf.bounding_box([pdf.bounds.left,
                      pdf.bounds.top * 0.4],
                     :width => pdf.bounds.width*0.65,
                     :height => pdf.bounds.height * 0.38) do
      pdf.image('logo.png',
                :at => [pdf.bounds.left, pdf.bounds.top],
                :fit => [pdf.bounds.width, pdf.bounds.height],
                :position => :center, :vposition => :center)
    end

    # Name, large, at the (almost) top
    pdf.bounding_box([pdf.bounds.left, pdf.bounds.top*0.9],
                     :width => pdf.bounds.width,
                     :height => pdf.bounds.height/3) do
      pdf.font_size = 18
      pdf.text person.name, :align => :center
    end

    # Nickname, a bit smaller, just below
    pdf.bounding_box([pdf.bounds.right*0.05, pdf.bounds.top*0.6],
                     :width => pdf.bounds.width*0.8,
                     :height => pdf.bounds.height/3) do
      pdf.font_size = 15
      pdf.text person.nickname, :align => :left
    end

    # Country, small, at the (almost) bottom), if present
    if person.country and !person.country.empty?
      pdf.bounding_box([pdf.bounds.right*0.05, pdf.bounds.top*0.45],
                       :width => pdf.bounds.width*0.8,
                       :height => pdf.bounds.height * 0.4) do
        pdf.font_size = 11
        pdf.text Country.for(person.country || ''), :align => :left
      end
    end

    # GPG data at the right bottom, if available
    if person.gpg
      pdf.bounding_box([pdf.bounds.left + pdf.bounds.width*0.65,
                        pdf.bounds.top*0.6],
                       :width => pdf.bounds.width*0.35,
                       :height => pdf.bounds.height*0.6) do

        fpr = Barby::QrCode.new(person.gpg[0])
        pdf.image(StringIO.new(fpr.to_png),
                  :at => [pdf.bounds.left, pdf.bounds.top],
                  :fit => [pdf.bounds.width, pdf.bounds.height],
                  :position => :center, :vposition => :center)

        pdf.bounding_box([pdf.bounds.left, pdf.bounds.top*0.9],
                         :width => pdf.bounds.width,
                         :height => pdf.bounds.height*0.2) do
          pdf.font_size = 10
          pdf.text 'KSP %s' % person.gpg[1], :align => :center
        end

      end
    end

  end
end

# Penta stores the countries in ISO-3166 (two letter) encoding. Get
# the "normal" name for each country. A country name might also be
# fully specified, in which case, we just render the received string
class Country
  @countries = {}
  `isoquery -c`.split(/\n/).each do |ctry|
    col=ctry.split /\t/
      @countries[col[0].downcase] = col[3].chomp
  end

  def self.for(ctry)
    return @countries[ctry] if @countries.has_key?(ctry)
    if ctry and ctry.size == 2
      @countries[ctry] = `isoquery #{ctry}`.split(/\t/)[3].chomp
    else
      @countries[ctry] = ctry
    end
  end
end

# ... And here we go!

Key.populate

if ARGV[0].nil? or ! File.exists? ARGV[0]
  puts ' *!* No data file specified. Using in-source list'
  att = [Person.new(1, '', '', '', ''),
         Person.new(2, '', '', '', ''),
         Person.new(3, '', '', '', ''),
         Person.new(4, '', '', '', ''),
         Person.new(5, '', '', '', ''),
         Person.new(6, '', '', '', ''),
         Person.new(7, '', '', '', ''),
         Person.new(8, '', '', '', ''),
         Person.new(9, '', '', '', ''),
         Person.new(10, '', '', '', ''),
        ]
else
  att = Attendance.new(ARGV[0])
end


p = Printout.new
p.render att

